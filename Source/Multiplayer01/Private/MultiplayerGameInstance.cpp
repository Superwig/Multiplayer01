#include "MultiplayerGameInstance.h"
#include "MainMenu.h"

#include "Engine/World.h"
#include "UObject/ConstructorHelpers.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"
#include "OnlineSessionSettings.h"

#undef LoadMenu

const static FName DefaultSessionName = TEXT("Session1");

UMultiplayerGameInstance::UMultiplayerGameInstance()
{
	static ConstructorHelpers::FClassFinder<UUserWidget> Widget(TEXT("/Game/UI/WBP_MainMenu"));
	checkf(Widget.Class, TEXT("MainMenu not found"))
		MenuWidgetBP = Widget.Class;
}

void UMultiplayerGameInstance::Init()
{
	Super::Init();
	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	if (Subsystem)
	{
		SessionInterface = Subsystem->GetSessionInterface();
		SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UMultiplayerGameInstance::HostSession);
		SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UMultiplayerGameInstance::CreateSession);
	}
}
void UMultiplayerGameInstance::HostSession(FName SessionName, bool bSuccess)
{
	if (bSuccess)
	{
		UE_LOG(LogTemp, Warning, TEXT("Hosting Session %s"), *SessionName.ToString())
		GetWorld()->ServerTravel("/Game/Maps/ThirdPersonExampleMap");
	}
}
void UMultiplayerGameInstance::LoadMenu()
{
	Menu = CreateWidget<UMainMenu>(this, MenuWidgetBP);
	if (Menu)
	{
		Menu->SetInterface(this);
	}
}

void UMultiplayerGameInstance::Host()
{
	if (SessionInterface.IsValid())
	{
		auto ExistingSession = SessionInterface->GetNamedSession(DefaultSessionName);
		if (ExistingSession)
		{
			//TBaseDelegate<void, FName, bool> Delegate;
			//Delegate.BindUObject(this, &UMultiplayerGameInstance::CreateSession);
			SessionInterface->DestroySession(DefaultSessionName);
		}
		else
		{
			CreateSession(DefaultSessionName, true);
		}
	}
}

void UMultiplayerGameInstance::CreateSession(FName SessionName, bool bSuccess)
{
	if (bSuccess)
	{
		FOnlineSessionSettings SessionSettings;
		SessionInterface->CreateSession(0, TEXT("Session1"), SessionSettings);
	}
}

void UMultiplayerGameInstance::Join(const FString& Addr)
{
	if (GetEngine())
	{
		GetEngine()->AddOnScreenDebugMessage(-1, 5.f, FColor::Cyan, FString::Printf(TEXT("%s is joining"), *Addr));
	}
}