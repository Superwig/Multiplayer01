#include "GameMenu.h"
#include "Components/Button.h"

bool UGameMenu::Initialize()
{
	bool SuperInit = Super::Initialize();
	if (!SuperInit)
	{
		return false;
	}
	checkf(MenuButton && BackButton, TEXT("GameMenu buttons not set up"))
	MenuButton->OnClicked.AddDynamic(this, &UGameMenu::LoadMainMenu);
	BackButton->OnClicked.AddDynamic(this, &UGameMenu::CloseGameMenu);
	return true;
}

void UGameMenu::LoadMainMenu()
{
	GetOwningPlayer()->ClientTravel(TEXT("/Game/Maps/MainMenu"), ETravelType::TRAVEL_Absolute);
}

void UGameMenu::CloseGameMenu()
{
	RemoveFromScreen();
}