#include "MPMenu.h"

void UMPMenu::AddToPlayerScreen(int32 ZOrder)
{
	Super::AddToPlayerScreen(ZOrder);
	FInputModeUIOnly UIMode;
	GetOwningPlayer()->SetInputMode(UIMode);
	GetOwningPlayer()->bShowMouseCursor = true;
}

void UMPMenu::RemoveFromScreen()
{
	RemoveFromViewport();
	FInputModeGameOnly GameMode;
	GetOwningPlayer()->SetInputMode(GameMode);
	GetOwningPlayer()->bShowMouseCursor = false;
}

void UMPMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	Super::OnLevelRemovedFromWorld(InLevel, InWorld);
	RemoveFromScreen();
}