#include "MPPlayerController.h"
#include "MPMenu.h"
#include "UObject/ConstructorHelpers.h"


AMPPlayerController::AMPPlayerController()
{

	static ConstructorHelpers::FClassFinder<UMPMenu> Widget(TEXT("/Game/UI/WBP_GameMenu"));
	checkf(Widget.Class, TEXT("GameMenu not found"))
	MenuWidgetBP = Widget.Class;
	
}

void AMPPlayerController::BeginPlay()
{
	Menu = CreateWidget<UMPMenu>(this, MenuWidgetBP);
}

void AMPPlayerController::SetupInputComponent()
{
	Super::SetupInputComponent();
	InputComponent->BindAction(TEXT("GameMenu"), IE_Pressed, this, &AMPPlayerController::ShowMenu);
}

void AMPPlayerController::ShowMenu()
{
	Menu->AddToPlayerScreen();	
}