#include "MainMenu.h"
#include "Components/Button.h"
#include "Components/EditableTextBox.h"
#include "Components/WidgetSwitcher.h"
#include "Components/CanvasPanel.h"
#include "Engine/Engine.h"

bool UMainMenu::Initialize()
{
	bool SuperInit = Super::Initialize();
	if (!SuperInit)
	{
		return false;
	}
	checkf(HostButton && JoinButton && JoinMButton && BackButton && QuitButton, TEXT("MainMenu buttons not set up"))
	HostButton->OnClicked.AddDynamic(this, &UMainMenu::Host);
	JoinMButton->OnClicked.AddDynamic(this, &UMainMenu::LoadJoinMenu);
	JoinButton->OnClicked.AddDynamic(this, &UMainMenu::Join);
	BackButton->OnClicked.AddDynamic(this, &UMainMenu::Back);
	QuitButton->OnClicked.AddDynamic(this, &UMainMenu::QuitGame);
	return true;
}

void UMainMenu::SetInterface(IMenuInterface* Interface)
{
	MenuInterface = Interface;
	AddToScreenAndSetFocus();
}

void UMainMenu::AddToScreenAndSetFocus()
{
	AddToPlayerScreen();
	HostButton->SetKeyboardFocus();
}

void UMainMenu::Host()
{
	checkf(MenuInterface, TEXT("MenuInterface not set"))
	MenuInterface->Host();
}

void UMainMenu::LoadJoinMenu()
{
	Switcher->SetActiveWidget(JoinMenuWidget);
	JoinInputText->SetKeyboardFocus();
}

void UMainMenu::Join()
{
	checkf(MenuInterface, TEXT("MenuInterface not set")) 
	MenuInterface->Join(JoinInputText->GetText().ToString());
}

void UMainMenu::Back()
{
	Switcher->SetActiveWidget(MainMenuWidget);
	HostButton->SetKeyboardFocus();
}

void UMainMenu::QuitGame()
{
	FGenericPlatformMisc::RequestExit(false);
}