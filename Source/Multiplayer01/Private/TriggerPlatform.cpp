// Fill out your copyright notice in the Description page of Project Settings.

#include "TriggerPlatform.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Materials/MaterialInstanceDynamic.h"

// Sets default values
ATriggerPlatform::ATriggerPlatform()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Base = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	TriggerPad = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("TriggerPad"));
	TriggerVolume = CreateDefaultSubobject<UBoxComponent>(TEXT("TriggerVolume"));

	RootComponent = Base;
	TriggerVolume->SetupAttachment(Base);
	TriggerPad->SetupAttachment(TriggerVolume);


	Base->SetMobility(EComponentMobility::Static);
	TriggerVolume->SetMobility(EComponentMobility::Static);
	TriggerPad->SetMobility(EComponentMobility::Movable);
}

// Called when the game starts or when spawned
void ATriggerPlatform::BeginPlay()
{
	Super::BeginPlay();
	PadMaterial = TriggerPad->CreateDynamicMaterialInstance(0);	
}

// Called every frame
void ATriggerPlatform::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

