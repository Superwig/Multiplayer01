#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MenuInterface.h"
#include "OnlineSessionInterface.h"
#include "MultiplayerGameInstance.generated.h"
#undef LoadMenu
/**
 * 
 */
UCLASS()
class MULTIPLAYER01_API UMultiplayerGameInstance : public UGameInstance, public IMenuInterface
{
	GENERATED_BODY()

public:
	UMultiplayerGameInstance();
	virtual void Init() override;
	UFUNCTION(BlueprintCallable)
	void LoadMenu();
	UFUNCTION(Exec)
	void Host();

	void CreateSession(FName SessionName, bool bSuccess);

	UFUNCTION(Exec)
	void Join(const FString& Addr);
private:
	void HostSession(FName SessionName, bool bSuccess);
	UPROPERTY()
	class UMainMenu* Menu;
	TSubclassOf<class UMainMenu> MenuWidgetBP;
	
	IOnlineSessionPtr SessionInterface;
};
