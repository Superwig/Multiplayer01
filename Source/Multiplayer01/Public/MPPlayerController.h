#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "MPPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER01_API AMPPlayerController : public APlayerController
{
	GENERATED_BODY()
public:
	AMPPlayerController();
protected:
	virtual void BeginPlay() override;
	virtual void SetupInputComponent() override;
private:
	void ShowMenu();
	TSubclassOf<class UMPMenu> MenuWidgetBP;
	class UMPMenu* Menu;
};
