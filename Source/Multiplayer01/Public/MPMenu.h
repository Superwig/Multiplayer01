

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MPMenu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER01_API UMPMenu : public UUserWidget
{
	GENERATED_BODY()
	
public:
	void AddToPlayerScreen(int32 ZOrder = 0);
	void RemoveFromScreen();
protected:
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;
};
