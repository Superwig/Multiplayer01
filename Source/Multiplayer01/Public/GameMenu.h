

#pragma once

#include "CoreMinimal.h"
#include "MPMenu.h"
#include "GameMenu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER01_API UGameMenu : public UMPMenu
{
	GENERATED_BODY()
	
public:
	virtual bool Initialize() override;
private:
	UPROPERTY(meta = (BindWidget))
	class UButton* MenuButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* BackButton;
	
	UFUNCTION()
	void LoadMainMenu();
	UFUNCTION()
	void CloseGameMenu();
};
