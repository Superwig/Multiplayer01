

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuInterface.h"
#include "MPMenu.h"
#include "MainMenu.generated.h"

/**
 * 
 */
UCLASS()
class MULTIPLAYER01_API UMainMenu : public UMPMenu
{
	GENERATED_BODY()
public:
	virtual bool Initialize() override;
	void SetInterface(IMenuInterface* Interface);
private:
	UFUNCTION()
	void Host();
	UFUNCTION()
	void LoadJoinMenu();
	UFUNCTION()
	void Back();
	UFUNCTION()
	void Join();
	UFUNCTION()
	void QuitGame();

	void AddToScreenAndSetFocus();

	UPROPERTY(meta = (BindWidget))
	class UWidgetSwitcher* Switcher;
	UPROPERTY(meta = (BindWidget))
	class UCanvasPanel* MainMenuWidget;
	UPROPERTY(meta = (BindWidget))
	class UButton* HostButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* JoinButton;
	UPROPERTY(meta = (BindWidget))
	class UCanvasPanel* JoinMenuWidget;
	UPROPERTY(meta = (BindWidget))
	class UButton* JoinMButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* BackButton;
	UPROPERTY(meta = (BindWidget))
	class UButton* QuitButton;
	UPROPERTY(meta = (BindWidget))
	class UEditableTextBox* JoinInputText;
	
	IMenuInterface* MenuInterface;
};
	