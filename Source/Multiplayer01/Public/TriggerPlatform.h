// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TriggerPlatform.generated.h"

UCLASS()
class MULTIPLAYER01_API ATriggerPlatform : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATriggerPlatform();

	// Called every frame
	virtual void Tick(float DeltaTime) override;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly)
	UStaticMeshComponent* TriggerPad;
	UPROPERTY(VisibleDefaultsOnly, BlueprintReadWrite)
	UMaterialInstanceDynamic* PadMaterial;
	UPROPERTY(EditInstanceOnly, BlueprintReadOnly)
	AActor* InteractableActor;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bHoldToActivate;
private:
	UPROPERTY(VisibleDefaultsOnly)
	UStaticMeshComponent* Base;
	UPROPERTY(VisibleDefaultsOnly)
	class UBoxComponent* TriggerVolume;
	
};
